<!DOCTYPE html>
<html>
<head>
	<title>Persona</title>
	<link rel="stylesheet" type="text/css" href="estilos/style.css">
</head>
<body>		
	<ul>
		<div class="logo">
			<img class="imgI"src="https://i.imgur.com/pjc37El.png">
				<li>CRUDING</li>
			<img class="imgD"src="https://i.imgur.com/sjvQolm.png">
		</div>
		<li><a href="Persona.php">Persona</a></li>
		<li><a href="Compra.php">Compra</a></li>
		<li><a href="Producto.php">Producto</a></li>  
	</ul>

    <?php
		require_once ('conexion.php');

		$listarPersona = $db->query("SELECT * FROM persona");
	?>

	<a href="Persona_Alta.php" class="boton">Nueva Persona</a>
	<table>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Nacimiento</th>
			<th>Borrar</th>
			<th>Editar</th>
		</tr>

		<?php while($datos = $listarPersona->fetch_assoc()) { ?>     

				<tr>
					<td>
						<?php echo $datos["id"] ?>
					</td>
					<td>
						<?php echo $datos["nombre"] ?>
					</td>
					<td>
						<?php echo $datos["apellido"] ?>
					</td>
					<td>
						<?php echo $datos["fechaNacimiento"] ?>
					</td>
					<td>
						<a href="Persona_Baja.php?borrar=<?php echo $datos['idRegistro']; ?>">
							<img class="icons" src="https://i.imgur.com/W2DvMnC.png">
						</a>
					</td>
					<td>
						<a href="Persona_Editar.php?editar=<?php echo $datos['idRegistro']; ?>">
							<img class="icons" src="https://i.imgur.com/1DCI8P6.png">
						</a>
					</td>
				</tr>
		
		<?php 
			}
			$db->close(); 
		?>		

	</tr>
	</table>
</body>
</html>
