<!DOCTYPE html>
<html>
<head>
	<title>Compra</title>
	<link rel="stylesheet" type="text/css" href="estilos/style.css">
</head>
<body>		
 	<ul>
		<div class="logo">
			<img class="imgI"src="https://i.imgur.com/pjc37El.png">
				<li>CRUDING</li>
			<img class="imgD"src="https://i.imgur.com/sjvQolm.png">
		</div>
		<li><a href="Persona.php">Persona</a></li>
		<li><a href="Compra.php">Compra</a></li>
		<li><a href="Producto.php">Producto</a></li>  
	</ul>

    <?php
		require_once ('conexion.php');

		$listarCompra = $db->query("SELECT * FROM Compra");
	?>

	<a class="boton" href="Compra_Alta.php" class="boton">Nueva Compra</a>

	<table>
		<tr>
			<th>ID Persona</th>
			<th>ID Producto</th>
			<th>Fecha - Hora</th>
			<th>Cantidad</th>
			<th>Borrar</th>
			<th>Editar</th>
		</tr>

		<?php while($datos = $listarCompra->fetch_assoc()) { ?>     

				<tr>
					<td>
						<?php echo $datos["idPer"] ?>
					</td>
					<td>
						<?php echo $datos["idProd"] ?>
					</td>
					<td>
						<?php echo $datos["fechaHora"] ?>
					</td>
					<td>
						<?php echo $datos["cantidad"] ?>
					</td>
					<td>
						<a href="Compra_Baja.php?borrar=<?php echo $datos['idRegistro']; ?>">
							<img class="icons" src="https://i.imgur.com/W2DvMnC.png">
						</a>
					</td>
					<td>
						<a href="Compra_Editar.php?editar=<?php echo $datos['idRegistro']; ?>">
							<img class="icons" src="https://i.imgur.com/1DCI8P6.png">
						</a>
					</td>
				</tr>
		
		<?php 
			}
			$db->close(); 
		?>		

	</tr>
	</table>
</body>
</html>
